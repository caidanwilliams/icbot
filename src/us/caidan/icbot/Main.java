package us.caidan.icbot;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.jibble.pircbot.IrcException;

import java.io.IOException;

/**
 * Created by Caidan Williams on 5/2/2016.
 */
public class Main extends Application {

    // Connection variables
    public static String server = "irc.chat.twitch.tv", password = "oauth:kmkg5xe0v4ocdlcotzsjyq7cs9xs74";
    public static int port = 6667;

    // New bot
    TwitchBot bot;

    //Channel var
    public static String userChannel = "#psychedelicpelican";

    // Main method
    public static void main(String[] args) {
        launch(args); // Fail safe for older devices and such
    }

    // Main GUI Stuff here
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("ICBot");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        // User Name
        Label userL = new Label("User Name:");
        grid.add(userL, 0, 1);

        TextField userTF = new TextField();
        grid.add(userTF, 1, 1);

        // Server
        Label serverL = new Label("Host Name:");
        grid.add(serverL, 0, 2);

        TextField serverTF = new TextField();
        grid.add(serverTF, 1, 2);

        // Channel
        Label channelL = new Label("Channel:");
        grid.add(channelL, 0, 3);

        TextField channelTF = new TextField();
        grid.add(channelTF, 1, 3);

        // Port
        Label portL = new Label("Port:");
        grid.add(portL, 0, 4);

        TextField portTF = new TextField();
        grid.add(portTF, 1, 4);

        // Password
        Label passwordL = new Label("Password:");
        grid.add(passwordL, 0, 5);

        TextField passwordTF = new TextField();
        grid.add(passwordTF, 1, 5);

        // Text to display info
        Text actiontarget = new Text();
        grid.add(actiontarget, 1, 10);

        // Label for checkbox
        Label saveBoxL = new Label("Save");
        grid.add(saveBoxL, 0, 6);

        // Checkbox for saving
        CheckBox saveBox = new CheckBox();
        grid.add(saveBox, 1, 6);

        // Button for connecting
        Button connectBtn = new Button("Connect");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(connectBtn);
        grid.add(hbBtn, 1, 8);

        // Label for delay checkbox
        Label delayCbL = new Label("Delay");
        grid.add(delayCbL, 3, 1);

        // Checkbox for Delay
        CheckBox delayCB = new CheckBox();
        grid.add(delayCB, 4, 1);

        // Label for delay
        Label delayL = new Label("Delay (in milliseconds)");
        grid.add(delayL, 3, 2);

        // Text field for delay
        TextField delayTF = new TextField();
        delayTF.setText("0");
        grid.add(delayTF, 4, 2);

        // Label for delay rand checkbox
        Label randDelayCbL = new Label("Randomize Delay");
        grid.add(randDelayCbL, 3, 3);

        // Checkbox for rand Delay
        CheckBox randDelayCB = new CheckBox();
        grid.add(randDelayCB, 4, 3);

        // Label for Message Delay Min
        Label delayMinL = new Label("Delay Min (in milliseconds)");
        grid.add(delayMinL, 3, 4);

        // Text for Message Delay Min
        TextField delayMinTF = new TextField();
        delayMinTF.setText("0");
        grid.add(delayMinTF, 4, 4);

        // Label for Message Delay Max
        Label delayMaxL = new Label("Delay Max (in milliseconds)");
        grid.add(delayMaxL, 3, 5);

        // Text for Message Delay Min
        TextField delayMaxTF = new TextField();
        delayMaxTF.setText("0");
        grid.add(delayMaxTF, 4, 5);

        // Button for applying
        Button applyBtn = new Button("Apply");
        HBox hbApplyBtn = new HBox(10);
        hbApplyBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbApplyBtn.getChildren().add(applyBtn);
        grid.add(hbApplyBtn, 4, 8);

        connectBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (!userTF.getText().equals("") && !serverTF.getText().equals("") && !portTF.getText().equals("") && !passwordTF.getText().equals("")) {
                    server = serverTF.getText(); // Assign user's server
                    userChannel = channelTF.getText(); // Assign user's channel
                    port = Integer.parseInt(portTF.getText()); // Assign user's port
                    password = passwordTF.getText(); // Assign user's password or OAuth
                    try {
                        startBot(userTF.getText()); // Start bot with user's name
                    } catch (Exception e) {
                        actiontarget.setFill(Color.FIREBRICK);
                        actiontarget.setText(userTF.getText() + " could not connect"); // Displays bot couldn't connect
                    }
                    if (bot.isConnected()) {
                        actiontarget.setFill(Color.FORESTGREEN);
                        actiontarget.setText(userTF.getText() + " connected successfully"); // Displays bot connected

                        if (saveBox.isSelected()) {
                            // TODO: Save stuff here
                        }
                    }
                } else {
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("Missing parameters");
                }
            }
        });

        applyBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                TwitchBot.sleepTime = Integer.parseInt(delayTF.getText());
                TwitchBot.sleepTimeMin = Integer.parseInt(delayMinTF.getText());
                TwitchBot.sleepTimeMax = Integer.parseInt(delayMaxTF.getText());

                if (delayCB.isSelected()) {
                    TwitchBot.isSleep = true;
                }
                else {
                    TwitchBot.isSleep = false;
                }

                if (randDelayCB.isSelected()) {
                    TwitchBot.randSleepTime = true;
                }
                else {
                    TwitchBot.randSleepTime = false;
                }
            }
        });

        Scene scene = new Scene(grid, 750, 450);
        primaryStage.setScene(scene);

        primaryStage.show();
    }

    public void startBot(String botName) throws IOException, IrcException {
        bot = new TwitchBot(botName); // Start the bot up
        if (bot.isConnected()) {
            bot.disconnect();
        }
        bot.connect(server, port, password); // Connect to the IRC Server
        bot.joinChannel(userChannel);
    }

}
