package us.caidan.icbot;

/**
 * Created by Caidan Williams on 5/1/2016.
 */
public class Scanner {

    public String currentQuestion, currentAnswer; // Initializes variables
    public boolean sendAnswer = false;

    Parser parser = new Parser(); // Make new us.caidan.icbot.Parser object "parser"

    // Scan message data
    public void scanText(String text) {
        int start, end;
        if (text.contains("[") && text.contains("]")) {
            start = text.indexOf("[");
            end = text.indexOf("]");

            if (text.length() > end+2) {
                currentQuestion = text.substring(end + 2, text.length());
                currentAnswer = parser.findAnswer(text.substring(start, end+1), currentQuestion);
                sendAnswer = true;
                System.out.println("question: " + currentQuestion + " answer: " + currentAnswer);
            }
            else {
                sendAnswer = false;
            }
        }
        else {
            sendAnswer = false;
        }
    }

}
