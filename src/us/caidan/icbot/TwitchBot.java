package us.caidan.icbot;

import org.jibble.pircbot.PircBot;

/**
 * Created by Caidan Williams on 5/2/2016.
 */
public class TwitchBot extends PircBot {

    Scanner scanner = new Scanner();

    // Bot Variables
    static int sleepTime = 1000, sleepTimeMin = 750, sleepTimeMax = 1000;
    static boolean randSleepTime = false, isSleep = false;

    public TwitchBot(String name){
        this.setName(name);
        this.setVerbose(true); // Enable a debugging output
    }

    public void onMessage(String channel, String sender, String login, String hostname, String message) {
        scanner.scanText(message); // Scan all messages
        if (scanner.sendAnswer) {
            if (isSleep) {
                if (randSleepTime) {
                    sleepTime = (int) (Math.random() * sleepTimeMax) + sleepTimeMin; //Randomize the sleep time
                    System.out.println("Sleep time: " + sleepTime);
                }

                try {
                    Thread.sleep(sleepTime); // Attempt sleep
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
            }
            sendMessage(Main.userChannel, scanner.currentAnswer); // Send message to server
            scanner.sendAnswer = false;
        }
    }

}
