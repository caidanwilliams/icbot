package us.caidan.icbot;

/**
 * Created by Caidan Williams on 5/1/2016.
 */
public class Parser {

    // Init all questions based on tag
    String[][] champions = {{"Alphabetically what is the name of the last champion in League of Legends?", "Zyra"}, {"How many champions have no cost related to all their abilities?", "11"}, {"Alphabetically what is the name of the first champion in League of Legends?", "Aatrox"}, {"During ____'s development, Riot designers commonly referred to ____ as the \"Supportle\".", "Lulu"}, {"How many champions in League of Legends are yordles?", "16"}, {"How many champions receive the \"Ninja\" buff in-game?", "4"}, {"What champion has the shortest name in League of Legends?", "Vi"}, {"What champion, beside Sivir, has a spell shield that will block one enemy's ability?", "Nocturne"}, {"Evelynn and ____ can be in stealth forever.", "Teemo"},{"What champion has no ultimate and instead has four basic abilities?", "Udyr"}, {"What champion has the highest base armor of 28.8?", "Rammus"}, {"A champion's name least commonly starts with which letter?", "Q"}, {"A champion's name most commonly starts with which letter?", "S"}, {"What champion has the lowest base health of 468 HP?", "Anivia"}, {"What champion name begins and ends with the letter R?", "Rengar"}, {"What champion name begins and ends with the letter A?", "Anivia"}, {"What champion name begins and ends with the letter E?", "Elise"}, {"Apart from Volibear, which champion has an ability that fears only minions and monsters?", "Darius"}, {"What champion has the longest one-word name in League of Legends?", "Fiddlesticks"}, {"How many champions utilize mana as their casting resource but still have abilities that cost health?", "2"}, {"How many champions use Health as their sole casting resource?", "5"}, {"Teemo is tagged as both a marksman and an ____.", "Assassin"}, {"____ was known for carrying a mace, is known as the Master of Metal, and was made out of sickness and darkness.", "Mordekaiser"}, {"Which champion possesses the longest hard crowd control effect for a single ability?", "Ashe"}, {"Aatrox, Akali, Alistar, Amumu, Anivia, Annie, Ashe, and Azir are all champions whose name starts with the letter A. Which other champion's name starts with the letter A?", "Ahri"}, {"____ is the smallest champion in League of Legends. ____'s size is 100 x 100 units, making him a good scale for estimating ranges.", "Teemo"}, {"How many champions have an ability that applies the healing reduction effect, Grievous Wounds?", "2"}, {"How many champions utilize the Fury as their secondary resource?", "4"}, {"____ and Kassadin are the only two champions that have abilities that persistently allow them to ignore unit collision.", "Fizz"}, {"Which common support is not tagged as a support?", "Blitzcrank"}, {"What champion has the highest base attack damage of 64 AD?", "Moakai"}, {"____ is the second champion to use the stance mechanic, the first being Udyr.", "Sona"}, {"How many champions use Energy as their sole casting resource?", "5"}, {"What champion, beside Janna, can boost their allies' attack damage with their abilities?", "Taric"}, {"What champion has the lowest base attack damage of 40 AD?", "Orianna"}, {"At what level does Tristana's basic attack range surpass Caitlyn's?", "16"}, {"____ is fierce, carrying two daggers, and goes by the name of The Sinister Blade.", "Katarina"}, {"What champion drops two souls for Thresh instead of one?", "Nunu"}, {"What champion has the highest base health of 626 HP?", "Tryndamere"}, {"____ shares a quote with Ezreal, Thresh, Irelia, and Evelynn; \"This way.\"", "Annie"}, {"What champion is the only one to transform without the use of their ultimate ability?", "Gnar"}, {"What champion has the lowest base armor of 16?", "Thresh"}, {"Teemo, Jhin, and ____ can all place stealthed objects.", "Shaco"}};
    String[][] history = {{"Who was the first top laner for Team SoloMid?", "Saintvicious"}, {"What was the last champion released in 2012?", "Vi"}, {"What was the last champion released in 2013?", "Yasuo"}, {"What was the first champion released in 2014?", "Velkoz"}, {"____ of Gold was an old item that gave 200 health and 5 gold per 10 seconds.", "Heart"}, {"What was the first champion released in 2012?", "Sejuani"}, {"In an old version of Summoner's Rift, the wolf and ____ camps randomly alternated.", "Golem"}, {"The elo rating system was replaced by the league system in Season ____.", "3"}, {"Tailsman of Ascension was originally named ____.", "Shurelyas reverie"}, {"Who was the first jungler for Counter Logic Gaming?", "Kobe"}, {"What was the first champion released in 2013?", "Thresh"}, {"What champion was the first to be released after the initial launch of League of Legends?", "Singed"}, {"What was the first champion released in 2015?", "Bard"}};
    String[][] lcs = {{"How many teams in the 2014 NA LCS season will be subject to relegation each split?", "3"}, {"SaintVicious has been part of three LCS teams:", "CLG"}, {"Voyboy has been part of three LCS teams: Curse, CLG, and ____.", "Dignitas"}, {"In Season 4, a tournament was setup for teams to fight their way into the ____ called the Challenger Series.", "LCS"}, {"Apart from CLG and TSM, Doublelift has been a member of what other team currently in the LCS?", "Team Liquid"}, {"How many teams are in the 2015 NA LCS?", "10"}, {"____ is the only 2014 NA LCS team without a roster change.", "C9"}};
    String[][] passives = {{"____'s passive converts 50% of his total magic resistance into ability power.", "Galio"}, {"____'s basic attacks apply a stack of Concussive Blows for 5 seconds. Upon reaching 4 stacks, the target is stunned.", "Braum"}, {"____'s passive increases his mana regeneration by 1.5% for each 1% of his missing mana.", "Veigar"}, {"____'s passive deals 20% bonus damage when striking a unit from behind with his basic attacks or abilities.", "Shaco"}, {"What champion's passive, known as Staggering Blows, roots enemy champions on basic attacks?", "Nautilus"}, {"____'s auto attacks reduce the target's magic resistance by 15/20/25 for 3 seconds.", "Amumu"}, {"Whenever an enemy unit dies, ____ heals for a percent of its maximum health.", "Trundle"}, {"Every 18 seconds, ____'s next ability costs no mana.", "Lissandra"}, {"____ gains a charge every time an enemy unit is hit with any of her spells. At 9 charges her next spell heals her based on the number of enemies hit.", "Ahri"}, {"____'s auto attacks reduce armor and magic resistance by 3% for 5 seconds, which can stack 5 times.", "Kayle"}, {"____'s passive allows him to recover health and mana whenever he kills a unit.", "Cho'gath"}, {"____'s passive builds up stacks once every 6 seconds.", "Cassiopeia"}, {"Every 12 seconds, ____'s next basic attack restores mana.", "Xerath"}};
    String[][] skins = {{"What female champion has the most skins?", "Annie"}, {"How many Legendary skins are there in League of Legends?", "23"}, {"What is the RP cost of the cheapest skin in League of Legends?", "260"}, {"How many Ultimate skins are there in League of Legends?", "3"}, {"How many champions have a \"traditional\" skin?", "4"}, {"____ Tristana is a skin given to any player who likes the Riot Facebook Page.", "Riot Girl"}, {"Teemo and ____ are the only two champions that have a Panda skin.", "Annie"}, {"Annie and ____ are the only two champions that have a Prom skin.", "Amumu"}, {"How many champions have a Battlecast skin?", "7"}, {"Victorious ____ was given to any players that reached Gold or higher during Season 1.", "Jarvan"}, {"Victorious ____ was given to any players that reached Gold or higher during Season 2.", "Janna"}, {"Victorious ____ was given to any players that reached Gold or higher during Season 3.", "Elise"}, {"____ Alistar is a skin given to any player who subscribes to the Riot YouTube channel.", "Unchained"}, {"____ Garen is a skin given to any player who follows the Riot Twitter account.", "Dreadknight"}, {"What male champion has the most skins?", "Ryze"}, {"Four Internet browsers are represented by the following skins: Foxfire Ahri, Safari Caitlyn, Chrome Rammus, and ____ Ezreal.", "Explorer"}, {"[Skins] ____ Ryze was a skin given to winners of official League of Legend Tournaments.", "Trophy"}, {"How many champions have a medical related skin?", "3"}};
    String[][] general = {{"Which jungle camp consists of four enemies?", "Raptors"}, {"In Summoner's Rift each team has ____ turrets, also called towers.", "11"}, {"After 35 minutes, one siege minion is spawned every ____ minion waves.", "2"}, {"In the first 35 minutes, one siege minion is spawned every ____ minion waves.", "3"}, {"The Runic Affinity mastery which increases buff durations, as of Season 6, does extend the duration of buffs obtained from ____ monsters.", "Epic"}, {"When the announcer notifies that either your or the enemy's inhibitor will respawn soon, said inhibitor will respawn in ____ seconds.", "15"}, {"Minion waves grow stronger every ____ waves.", "3"}, {"What projectiles will still go through projectile-blocking abilities like Yasuo's Windwall and Braum's Unbreakable?", "Turret Shots"}, {"What champion used to have an ability that shared its in-game icon with that of the \"Exalted with Baron Nashor\" buff?", "Kassadin"}, {"What does IP stand for?", "Influence points"}, {"What does RP stand for?", "Riot points"}, {"Inhibitors respawn ____ minutes after they are destroyed.", "5"}, {"How many minutes does it take from dragon to respawn?", "6"}, {"Alistar's Trample and ____'s Tremors are the only two Area of Effect abilities that damage structures.", "Rammus"}, {"Alternative resources to Mana include Health, Energy, Ferocity, Fury, Rage, and ____.", "Heat"}, {"How many champions are currently in League of Legends?", "130"}, {"What is the name of the large boss on Twisted Treeline?", "Vilemaw"}};
    String[][] loading_tips = {{"Did you know that it's not a good idea to do ____ when the opposite team is alive?", "Baron"}, {"Did you know the longer the game goes on, the ____ chance the losing team has of making a comeback?", "Greater"}, {"Did you know that it's not a good idea to ____ Singed?", "Chase"}, {"Did you know that Demacia prizes itself as the ____ of the righteous and just?", "Home"}, {"The ____ is a system that has improved the behaviors of over 280,000 players in the past 12 months.", "Tribunal"}, {"Did you know that ____ win games?", "Wards"}, {"78% of the players punished in the Tribunal ____ their behavior and are never punished again.", "Improve"}, {"If you need 50g, kill the enemy ____.", "Nexus"}, {"Did you know that it's a good idea to ____ your AD carry?", "protect"}, {"The community overwhelmingly votes to ____ homophobic or racial slurs in the Tribunal system.", "Punish"}};
    String[][] ranked_ladder = {{"If you don't play a game within ____ days your LP will begin to decay.", "28"}, {"How many tiers are there in the League of Legends ladder?", "7"}, {"The ____ tier and below do not lose LP when they become inactive after 28 days.", "Gold"}, {"Unlike Season 3, in Season 4 it is possible for Summoners to be demoted from both divisions and ____.", "Tiers"}, {"How many players can be on one ranked team?", "9"}, {"How many games does a ranked team need to play before they are placed into a tier?", "5"}, {"How many champions does a Summoner need to own in order to play Ranked games?", "16"}, {"What does LP stand for?", "League points"}, {"When a team or player in Diamond Tier goes inactive they lose 50 LP per week while a Platinum Tier team or player only loses ____ LP per week.", "35"}, {"How many players are allowed in the Challenger Tier?", "200"}, {"In Season 3, ____ had multiple accounts in challenger.", "Wildturtle"}};
    String[][] abilities = {{"____'s ability Steadfast Presence has a passive that increases her armor and magic resistance.", "Poppy"}, {"____'s E raises a shield that reduces incoming damage and intercepts oncoming projectiles.", "Braum"}, {"____'s Q propels ice from his shield in a straight line. The first enemy hit is slowed by 70% decaying over 2 seconds.", "Braum"}, {"____'s Ultimate briefly knocks up enemies in a line, but the first champion hit is knocked up for 1.5 seconds.", "Braum"}, {"____'s W gives him a shield for 2 seconds and a quick burst of speed for 1 second.", "Rumble"}, {"Sona's Aria of Perseverance not only heals, but it also ____ herself and any allied champions tagged by its aura.", "Shields"}, {"Only Lux, Morgana, and ____ have abilities with the word \"Binding\" in the name.", "Bard"}, {"Whenever Poppy uses her Ultimate ability to knock enemies back toward their fountain she continues to maintain ____ of them.", "Vision"}, {"____'s ability Terror Capacitor gives him a shield. While the shield is active his auto attacks slow his targets.", "Urgot"}, {"____'s W makes him leap to the aid of a nearby ally, positioning himself between his target and the enemy champion closes to them.", "Braum"}};
    String[][] item_stats = {{"Zyra and Ryze are only two champions in League of Legends with abilities that give this stat.", "Cooldown reduction"}, {"Veigar and Thresh are the only two champions who theoretically can obtain infinite ____.", "Ability Power"}, {"Every point of ____ requires a unit to take 1% more of its maximum health in physical damage to be killed.", "Armor"}, {"____ is a stat that benefits from damage done via activated items, summoner spells, and abilities.", "Spell vamp"}, {"____ is the most expensive stat in the game costing 35 gold per each point.", "Attack damage"}, {"____ is a stat that benefits from basic attacks, on-hit effects, and abilities that modify your next basic attack.", "Life steal"}, {"Darius, Draven, Garen, Kalista, Olaf, Riven, Talon, Vayne, and Zed have no abilities or innate effects that scale with ____.", "Ability power"}, {"____ is a stat that converts a percentage of an ability's damage dealt into health.", "Spell vamp"}};
    String[][] items = {{"This boot enchantment grants 12% move speed decaying over 2 seconds when dealing damage from a single target attack or spell.", "Furor"}, {"This boot enchantment grants +20 movement speed.", "Alacrity"}, {"How many items provide a Unique Aura that affects either nearby allies or enemies?", "6"}, {"____ is part of the recipe for both Athene's Unholy Grail and Mikael's Crucible.", "Chalice of Harmony"}, {"According to LolKing, ____ is the most popular major item.", "Infinity edge"}, {"____ is the most expensive item that grants Ability Power.", "Rabadons deathcap"}, {"____'s unique passive allows your champion to ignore unit collision.", "Phantom dancer"}, {"____'s passive grants 10% base health regeneration for up to 10 seconds after taking damage from an enemy champion.", "Spectres cowl"}, {"What is the cheapest non-consumable item in League of Legends?", "Faerie charm"}, {"This boot enchantment decreases the cooldown of Flash, Ghost, and Teleport by 20% and grants each spell an additional effect.", "Distortion"}, {"The shield from Banshee's Veil refreshes after no damage is taken from enemy champions for ____ seconds?", "40"}, {"How much health does a Health Potion restore over 15 seconds?", "150"}, {"____'s passive must have a value of 420 gold for this item to be efficient, even if it does revive your champion upon death restoring health and mana.", "Gaurdian Angel"}};
    String[][] summoner_spells = {{"How much of a shield does Summoner Barrier provide at level 18?", "455"}, {"How many Summoner Spells are there in League of Legends?", "13"}, {"Summoner Heal not only heals but grants __% move speed for 1 seconds.", "30"}, {"How much damage does Summoner Ignite do at level 1?", "70"}, {"How much health is restored by Summoner Heal at level 18?", "345"}, {"Summoner Ghost grants __% move speed over 10 seconds.", "27"}, {"Between Summoner Heal, Barrier, and Ignite which has the longest cooldown?", "Heal"}, {"How much of a shield does Summoner Barrier provide at level 1?", "115"}, {"How much damage does Summoner Ignite do at level 18?", "410"}, {"____ is the Summoner Spell with the shortest cooldown of 60 seconds (excluding Smite which has a cooldown of 15 seconds when there's a charge available).", "Clairvoyance"}};
    String[][] twitchtv = {{"What TwitchTV emote features the head of a Beagle?", "FrenkerZ"}, {"What greek letter is also a TwitchTV emoticon?", "Kappa"}, {"What TwitchTV emote features a pink face with tears?", "BibleThump"}};
    String[][] store = {{"How much RP does a Rune Page cost?", "590"}, {"How much IP does a Rune Page cost?", "6300"}, {"Skins and champions are usually on sale for ____ days?", "4"}, {"What is the oldest champion that still costs 6300 IP?", "Darius"}, {"A Summoner Name Change can be purchased with ____ IP.", "13900"}, {"____ skins and ____ champions are on sale at almost any given point in time.", "4"}, {"How much IP is the most expensive Tier 3 Rune?", "2050"}, {"How much IP is the least expensive Tier 3 Rune?", "102"}};
    String[][] masteries = {{"What is the name of the Cunning Mastery that restores 5% of your missing health and mana on champion kill or assist?", "Dangerous game"}, {"What is the name of the Resolve Mastery that reduces the cooldown of Summoner Spells by 15%?", "Insight"}, {"What is the name of the Ferocity Mastery that provides up to 2% increased ability damage?", "Sorcery"}, {"What is the name of the Resolve Mastery that increases Tenacity and Slow Resist?", "Swiftness"}, {"What is the name of the Cunning Mastery that increases the cooldown reduction cap to 45%?", "Intelligence"}, {"What is the name of the Ferocity Mastery that provides up to 4% attack speed?", "Fury"}, {"What is the name of the Cunning Mastery that increases the duration of neutral monster buffs by 15%?", "Runic affinity"}, {"What is the name of the Ferocity Mastery that provides percentage magic penetration?", "Piercing thoughts"}, {"What is the name of the Resolve Mastery that increases bonus armor and magic resist?", "Unyielding"}, {"What is the name of the Ferocity Mastery that grants life steal based on your missing health?", "Warlords bloodlust"}, {"What is the name of the Ferocity Mastery that provides percentage armor penetration?", "Battering blows"}, {"What is the name of the Cunning Mastery that grants 35% movement speed after dealing 30% of an enemy champions max health within 2 seconds?", "Stormraiders surge"}, {"What is the name of the Resolve Mastery that steals an emeny champion's health on auto attack?", "Grasp of the undying"}, {"What is the name of the Resolve Mastery that absorbs damage taken from the nearest allied champion?", "Bond of Stone"}};

    // If we find the tag, then loop through its array until we find the right question, then produce its answer
    public String findAnswer(String tag, String question) {
        String answer = "";

        switch (tag) {
            case "[Champions]":
                for (int i = 0; i < champions.length; i++) {
                    if (champions[i][0].equals(question)) {
                        answer = champions[i][1];
                        break;
                    }
                }
                break;
            case "[History]":
                for (int i = 0; i < history.length; i++) {
                    if (history[i][0].equals(question)) {
                        answer = history[i][1];
                        break;
                    }
                }
                break;
            case "[Masteries]":
                for (int i = 0; i < masteries.length; i++) {
                    if (masteries[i][0].equals(question)) {
                        answer = masteries[i][1];
                        break;
                    }
                }
                break;
            case "[LCS]":
                for (int i = 0; i < lcs.length; i++) {
                    if (lcs[i][0].equals(question)) {
                        answer = lcs[i][1];
                        break;
                    }
                }
                break;
            case "[Passives]":
                for (int i = 0; i < passives.length; i++) {
                    if (passives[i][0].equals(question)) {
                        answer = passives[i][1];
                        break;
                    }
                }
                break;
            case "[Skins]":
                for (int i = 0; i < skins.length; i++) {
                    if (skins[i][0].equals(question)) {
                        answer = skins[i][1];
                        break;
                    }
                }
                break;
            case "[General]":
                for (int i = 0; i < general.length; i++) {
                    if (general[i][0].equals(question)) {
                        answer = general[i][1];
                        break;
                    }
                }
                break;
            case "[Loading Tips]":
                for (int i = 0; i < loading_tips.length; i++) {
                    if (loading_tips[i][0].equals(question)) {
                        answer = loading_tips[i][1];
                        break;
                    }
                }
                break;
            case "[Ranked Ladder]":
                for (int i = 0; i < ranked_ladder.length; i++) {
                    if (ranked_ladder[i][0].equals(question)) {
                        answer = ranked_ladder[i][1];
                        break;
                    }
                }
                break;
            case "[Abilities]":
                for (int i = 0; i < abilities.length; i++) {
                    if (abilities[i][0].equals(question)) {
                        answer = abilities[i][1];
                        break;
                    }
                }
                break;
            case "[Item Stats]":
                for (int i = 0; i < item_stats.length; i++) {
                    if (item_stats[i][0].equals(question)) {
                        answer = item_stats[i][1];
                        break;
                    }
                }
                break;
            case "[Items]":
                for (int i = 0; i < items.length; i++) {
                    if (items[i][0].equals(question)) {
                        answer = items[i][1];
                        break;
                    }
                }
                break;
            case "[Summoner Spells]":
                for (int i = 0; i < summoner_spells.length; i++) {
                    if (summoner_spells[i][0].equals(question)) {
                        answer = summoner_spells[i][1];
                        break;
                    }
                }
                break;
            case "[TwitchTV]":
                for (int i = 0; i < twitchtv.length; i++) {
                    if (twitchtv[i][0].equals(question)) {
                        answer = twitchtv[i][1];
                        break;
                    }
                }
                break;
            case "[Store]":
                for (int i = 0; i < store.length; i++) {
                    if (store[i][0].equals(question)) {
                        answer = store[i][1];
                        break;
                    }
                }
                break;
            default:
                answer = "";
                break;
        }

        return answer;
    }

}
